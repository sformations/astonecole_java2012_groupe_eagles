package com.eecole.model;


public class Adresse {
    
    private int idUser;
    private String libelle;
    private int codePostal;
    private String ville;

    public Adresse(int idUser, String libelle, int codePostal, String ville) {
        this.idUser = idUser;
        this.libelle = libelle;
        this.codePostal = codePostal;
        this.ville = ville;
    }
    
    
   
}

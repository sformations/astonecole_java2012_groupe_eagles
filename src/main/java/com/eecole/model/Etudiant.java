package com.eecole.model;

import java.util.Calendar;
import java.util.Date;

public class Etudiant extends User {


    private Date dateNaissance;
    private int idPromotion;

    public Etudiant() {
        super();
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public int getIdPromotion() {
        return idPromotion;
    }

    public void setIdPromotion(int idPromotion) {
        this.idPromotion = idPromotion;
    }

    public Etudiant(int idUser, String nom, String prenom, String email, String photo, String login, String password, String libelle, int codePostal, String ville,int idPromotion) {
        super(idUser, nom, prenom, email, photo, login, password, libelle, codePostal, ville);
        this.idPromotion = idPromotion;
        dateNaissance = new Date();
    }
   
    public void consulterCours(String cours) {
    }

    public void consulterAbsence(Date dateAbsence) {
        Absence Absence1 = new Absence();
    }

    public void consulterListePromotion() {
        Promotion p1 = new Promotion();
    }
}
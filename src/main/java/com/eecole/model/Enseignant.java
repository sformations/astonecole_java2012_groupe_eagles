package com.eecole.model;

import java.util.Date;
import java.util.ArrayList;

public class Enseignant extends User {

    public Enseignant() {
        super();
    }

    public Enseignant(int idUser, String nom, String prenom, String email, String photo, String login, String password, String libelle, int codePostal, String ville) {
        super(idUser, nom, prenom, email, photo, login, password, libelle, codePostal, ville);
    }


    public void uploaderCours() {
    }

    public void saisieDesAbsences(int idPromotion, int idCours) {
       
    }

    public void consulterListeEtudiants(ArrayList listEtudiants, Etudiant e1) {
        if (listEtudiants.equals(e1)) {
            listEtudiants.toArray();
        }
    }

}
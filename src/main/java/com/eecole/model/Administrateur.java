package com.eecole.model;

import java.util.Date;
import java.io.File;
import java.util.ArrayList;

public class Administrateur extends User {

    public Administrateur(int idUser, String nom, String prenom, String email, String photo, String login, String password, String libelle, int codePostal, String ville) {
        super(idUser, nom, prenom, email, photo, login, password, libelle, codePostal, ville);
    }
   
    public Administrateur() {
        super();
    }
    
}

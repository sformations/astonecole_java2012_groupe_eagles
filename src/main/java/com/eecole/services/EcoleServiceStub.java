package com.eecole.services;

import com.eecole.model.User;
import com.eecole.model.Administrateur;
import com.eecole.model.Etudiant;
import com.eecole.model.Enseignant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.faces.model.ListDataModel;

public class EcoleServiceStub implements EcoleService {

    private static final Map<String, List<? extends User>> TYPE_USER_ID = new TreeMap<String, List<? extends User>>();
   
    private static final String ADMIN_ID = "01";
    private static final String ETUDIANT_ID = "02";
    private static final String ENSEIGNANT_ID = "03";
       public static final String ADMIN_TYPE = "ADMIN";
    public static final String ETUDIANT_TYPE = "ETUDIANT";
    public static final String ENSEIGNANT_TYPE = "ENSEIGNANT";
       private String typeDeUser;


    User user = new User();

    static {
        TYPE_USER_ID.put(
                ADMIN_ID,
                new ArrayList<Administrateur>(Arrays.asList(
                new Administrateur(1, "DUPONT", "PIERRE", "pdupont@ecole.com", "photoAdmin.jpg", "RTGRTG", "GRG", "RGRG", 75, "RGRG"),
                new Administrateur(2, "DUPONT1", "PIERRE1", "pdupont1@ecole.com", "photoAdmin1.jpg", "RGG", "RGRG", "RGRG", 75, "RGRG"))));

        TYPE_USER_ID.put(
                ETUDIANT_ID,
                new ArrayList<Etudiant>(Arrays.asList(
                new Etudiant(10, "MZALOUAT", "Ali", "ali@ecole.com", "photoAli.jpg", "", "", "", 75, "", 10),
                new Etudiant(11, "MARTIN", "Philippe", "philippe@ecole.com", "photoPhilippe.jpg", "", "", "", 75, "", 10))));

        TYPE_USER_ID.put(
                ENSEIGNANT_ID,
                new ArrayList<Enseignant>(Arrays.asList(
                new Enseignant(50, "NICHELE", "SEBASTIEN", "snichene@ecole.com", "photoSebastien.jpg", " ", "", "rue jean jaures 92260", 75, ""),
                new Enseignant(51, "BIDOCHON", "JEAN-CLAUDE", "jcbidochon@ecole.com", "photoJeanClaude.jpg", "  ", "", "rue gabriel perie 91400", 75, ""))));

    }
 

    public List<  User> listerUser() 
    
           
    {    
           
       List<User> listUsers = new ArrayList<User>();
        for (List<? extends User> listWork : TYPE_USER_ID.values()) {
            listUsers.addAll(listWork);
        }
       listUsers.toArray();
        return listUsers;
       
    
    }

    public List<Administrateur> listerAdmin() {

     return (List<Administrateur >) TYPE_USER_ID.get(ADMIN_ID);
    }
      public List<Etudiant> listerEtudiant() {

      return (List< Etudiant >) TYPE_USER_ID.get(ETUDIANT_ID);

      }
      public List<Enseignant> listerEnseignant() {

      return (List<Enseignant >) TYPE_USER_ID.get(ENSEIGNANT_ID);

    }
    
      
    @Override
   public List<? extends User> ajouterAdmin(Administrateur admin) {
        
     ((List<Administrateur >) TYPE_USER_ID.get(ADMIN_ID)).add(admin );   
        return TYPE_USER_ID.get(ADMIN_ID);
    }
    
    @Override
    public List<? extends User> ajouterEtudiant(Etudiant etudiant) {
        ((List<Etudiant>)TYPE_USER_ID.get(ETUDIANT_ID)).add(etudiant);
        return TYPE_USER_ID.get(ETUDIANT_ID);
    }
    
    @Override
    public List<? extends User> ajouterEnseignant(Enseignant enseignant) {
        ((List<Enseignant>)TYPE_USER_ID.get(ENSEIGNANT_ID)).add(enseignant);
        return TYPE_USER_ID.get(ENSEIGNANT_ID);
    }

    public String miseAjourUser(int index, User user) {
        List<User> listUsers = new ArrayList<User>();
        listUsers.set(index, user);
        return "miseAJour ok";
    }

  
   
    public List<? extends User> supprimerUser(Administrateur admin) 
    {
       TYPE_USER_ID.remove(ADMIN_ID);
       
       
        return TYPE_USER_ID.get(ADMIN_ID);
    }
}

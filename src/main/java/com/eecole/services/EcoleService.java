package com.eecole.services;

import com.eecole.model.Administrateur;
import com.eecole.model.Enseignant;
import com.eecole.model.Etudiant;
import com.eecole.web.AjoutUserBean;
import com.eecole.model.User;
import java.util.ArrayList;
import java.util.List;


public interface EcoleService {

    List<? extends User> ajouterAdmin(Administrateur admin);
    List<? extends User> ajouterEtudiant(Etudiant etudiant);
    List<? extends User> ajouterEnseignant(Enseignant enseignant);

    String miseAjourUser(int index, User user);
      List<Administrateur> listerAdmin();
     List<Etudiant> listerEtudiant();
     List<Enseignant> listerEnseignant();
      
   List<? extends User>  supprimerUser(Administrateur admin);
     List<  User> listerUser(); 
}

package com.eecole.web;




import com.eecole.model.Administrateur;
import com.eecole.model.User;
import com.eecole.services.*;


import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Administrateur
 */
public class SupprimerUserBean {
    private EcoleService ecoleService;
    //private String deleteUser;
    private User currentUser;
    private String nom;
    private String prenom;
    private int idUser;
    private String email;
    private String photo;
    private User selectedUser;
     
    private String login;
    private String password;
    private String libelle;
    private int codePostal;
    private String ville;
    private String typeDeUser;
    private int idPromotion;
    private String typeUser;
     private Administrateur admin;
    private DataModel dataModel;
    
    

     public DataModel getDataModel() 
     {
          ecoleService.supprimerUser(admin);
        return dataModel;
    }

    public void setDataModel(DataModel dataModel) {
        this.dataModel = dataModel;
    }
   
   
    public SupprimerUserBean() {
       
       
         
         
    }

    public EcoleService getEcoleService() {
        return ecoleService;
    }

    public void setEcoleService(EcoleService ecoleService) {
        this.ecoleService = ecoleService;
    }

    
  
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getTypeDeUser() {
        return typeDeUser;
    }

    public void setTypeDeUser(String typeDeUser) {
        this.typeDeUser = typeDeUser;
    }

    public int getIdPromotion() {
        return idPromotion;
    }

    public void setIdPromotion(int idPromotion) {
        this.idPromotion = idPromotion;
    }

    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }
    
    
    
    
    
}

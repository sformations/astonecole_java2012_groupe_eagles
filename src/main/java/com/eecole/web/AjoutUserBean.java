/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eecole.web;

import com.eecole.model.Administrateur;
import com.eecole.model.Enseignant;
import com.eecole.model.Etudiant;
import com.eecole.model.User;
import com.eecole.services.*;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.context.FacesContext;

/**
 *
 * @author Administrateur
 */
public class AjoutUserBean {

    public static final String ADMIN_TYPE = "ADMIN";
    public static final String ETUDIANT_TYPE = "ETUDIANT";
    public static final String ENSEIGNANT_TYPE = "ENSEIGNANT";
    private EcoleService ecoleService;
    private DataModel dataModel;
    private User currentUser;
    private String nom;
    private String prenom;
    private int idUser;
    private String email;
    private String photo;
    private String login;
    private String password;
    private String libelle;
    private int codePostal;
    private String ville;
    private String typeDeUser;
    private int idPromotion;
    private String typeUser;

   
    @PostConstruct
    public void initialisation() {
        this.ecoleService = new EcoleServiceStub();
        currentUser = new User();
    }



    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }

    public String verifAjoutUser() {

        if (ADMIN_TYPE.equals(typeDeUser)) {
            ecoleService.ajouterAdmin(new Administrateur(idUser, nom, prenom, email, photo, login, password, libelle, codePostal, ville));

            return "verifadminOk";
        } else if (ETUDIANT_TYPE.equals(typeDeUser)) {
            ecoleService.ajouterEtudiant(new Etudiant(idUser, nom, prenom, email, photo, login, password, libelle, codePostal, ville, idPromotion));

            return "verifetudiantOk";
        } else if (ENSEIGNANT_TYPE.equals(typeDeUser)) {
            ecoleService.ajouterEnseignant(new Enseignant(idUser, nom, prenom, email, photo, login, password, libelle, codePostal, ville));

            return "verifenseignantOk";
        } else {
            return null;
        }

    }

    public EcoleService getEcoleService() {
        return ecoleService;
    }

    public void setEcoleService(EcoleService ecoleService) {
        this.ecoleService = ecoleService;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {

        this.currentUser = currentUser;
    }



    public void setDataModel(DataModel dataModel) {
        this.dataModel = dataModel;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public int getIdUser() {
        return idUser;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoto() {
        return photo;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getLibelle() {
        return libelle;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getTypeDeUser() {
        return typeDeUser;
    }

    public void setTypeDeUser(String typeDeUser) {
        this.typeDeUser = typeDeUser;
    }

    public int getIdPromotion() {
        return idPromotion;
    }

    public void setIdPromotion(int idPromotion) {
        this.idPromotion = idPromotion;
    }

}

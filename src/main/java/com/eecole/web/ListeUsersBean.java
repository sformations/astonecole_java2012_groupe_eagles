/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eecole.web;
import com.eecole.services.*;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;


public class ListeUsersBean  {
    private EcoleService ecoleService;
    private DataModel dataModel3;
    private DataModel dataModel1;
     private DataModel dataModel2;
     private DataModel dataModel4;

    public DataModel getDataModel4() {
        dataModel4 = new ListDataModel(ecoleService.listerUser( ));
        return dataModel4;
    }

    public void setDataModel4(DataModel dataModel4) {
        this.dataModel4 = dataModel4;
    }
    

    
  

  public ListeUsersBean() {
     
    }
 
 

    public DataModel getDataModel1() {
        dataModel1 = new ListDataModel(ecoleService.listerAdmin());
         return dataModel1;
         
                             }
 public DataModel getDataModel3() {
        dataModel3 = new ListDataModel(ecoleService.listerEtudiant());
         return dataModel3;
 }
         
    

    public void setDataModel3(DataModel dataModel2) {
        this.dataModel3 = dataModel2;
        
    }
 public DataModel getDataModel2() {
        dataModel2 = new ListDataModel(ecoleService.listerEnseignant());
         return dataModel2;
 }
         
    

    public void setDataModel2(DataModel dataModel2) {
        this.dataModel2 = dataModel2;
        
    }
    

    public void setDataModel1(DataModel dataModel1) {
         this.dataModel1= dataModel1;
    }
  

    public void setEcoleService(EcoleServiceStub ecoleService) {
        this.ecoleService = ecoleService;
    }

    
}
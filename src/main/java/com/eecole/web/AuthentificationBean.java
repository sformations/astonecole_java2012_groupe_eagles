
package com.eecole.web;
import com.eecole.model.User;
import javax.faces.context.FacesContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AuthentificationBean {
    private String login;
    private String password;
    private User currentUser;
 
    
    public AuthentificationBean(){
    
    }
    
    public String authentificate(){
       
       if(("admin".equals(login)) && "admin".equals(password)){
              return"adminOk";}
         else if(("etudiant".equals(login)) && "etudiant".equals(password)){
              return"etudiantOk";}
         else if(("enseignant".equals(login)) && "enseignant".equals(password)){
              return"enseignantOk";}
       
         else {return "authentificateKo";}
        
    }
  
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

}
  